import { ApolloServer } from 'apollo-server-express';
import express, { Application } from 'express';
import { createServer, Server } from 'http';
import cors from 'cors';
import typeDefs from './graphql/typedefs';
import resolvers from './graphql/resolvers';

const serverLogCallback = function(websockerSrv: ApolloServer) {
  return function() {
    console.log(`🚀 REST Http server is listening at http://localhost:3000`);
    console.log(`🚀 GraphQL Websocket server is listening at http://localhost:4000${websockerSrv.graphqlPath}`);
  }
}
const apolloExpressServer = async () => {
  // Express http server for non-websocket request/response
  const app:Application = express();
  const httpServer:Server = createServer(app);
  app.use(cors({ origin: "http://localhost:3000", credentials: true }));

  // Apollo websocket graphql server for all chat requests/response
  const apolloServer:ApolloServer = new ApolloServer({ typeDefs, resolvers });

  // We start the apollo graphql server and apply other middleware
  await apolloServer.start();
  apolloServer.applyMiddleware({ app, cors: false });
  httpServer.listen(process.env.PORT, serverLogCallback(apolloServer));
}

apolloExpressServer()
.catch((err: Error) => { console.log(`Failed starting server: ${err}`) });
