import { Schema, model } from 'mongoose';
import User from '../user/user';

const RoomSchema = new Schema({
    _id: { type: ObjectId },
    name: { type: String },
    isPrivate: { type: Boolean },
    members: { type: [User] } // array of users
});

export default model('Room', RoomSchema);
