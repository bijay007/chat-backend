import { Schema, model } from 'mongoose';

const UserSchema = new Schema({
    _id: { type: ObjectId },
    username: { type: String },
    email: { type: String },
    password: { type: String }, // encoded password
});

export default model('User', UserSchema);
