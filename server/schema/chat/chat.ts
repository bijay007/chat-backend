import { Schema, model } from 'mongoose';

const ChatSchema = new Schema({
    _id: { type: ObjectId },
    sender: { type: String },
    receiver: { type: String },
    roomId: { type: String }, // _id from room table
    createdAt: { type: Date },
    deletedAt: { type: Date },
    updatedAt: { type: Date },
    message: { type: String }
});

export default model('Chat', ChatSchema);
