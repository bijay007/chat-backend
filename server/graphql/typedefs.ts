import { gql } from 'apollo-server-express';

const typeDefs = gql`
  # schema for User
  type User {
    id: ID!
    username: String!
    email: String!
    password: String!
  }

  # schema to query for Users
  type Query {
    getUsers: [User] # returns an array of User
  }
`
export default typeDefs;
