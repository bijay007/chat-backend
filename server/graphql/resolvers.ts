const mockUsers = [
  {
    _id: 1,
    name: 'Bijay',
    email: 'bijay@gmail.com',
    password: 'xyz123'
  }
]
const resolvers = {
  Query: {
    getUsers: () => mockUsers
  }
};

export default resolvers;
